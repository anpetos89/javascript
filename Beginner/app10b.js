const Punto = {
  init: function init(x, y) {
    let obj = Object.create(this);
    obj.x = x;
    obj.y = y;
    return obj;
  },
  moverEnX: function moverEnX(x) {
    this.x += x;
  },
  movenEnY: function movenEnY(y) {
    this.y += y;
  },
  distancia: function distancia(p) {
    const x = this.x - p.x;
    const y = this.y - p.y;
    return Math.sqrt(Math.pow(x,2) + Math.pow(y,2));
  }
}

const p1 = Punto.init(0, 4);
const p2 = Punto.init(3, 0);

console.log(p1.distancia(p2));
console.log(p2.distancia(p1));
p1.moverEnX(10);
console.log(p1.distancia(p2));
p2.movenEnY(-4);
console.log(p1.distancia(p2));
