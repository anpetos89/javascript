// Scope and closure
function saludarASacha10() {
  const nombre = "Sacha"
  for (let i = 0; i < 10; i++) {
    console.log(`Hola ${nombre}`);
  }
  /*for (var i = 0; i < 10; i++) {
    console.log(`Hola ${nombre}`);
  }*/
  //console.log(`El valor de i es: ${i}`);
}
saludarASacha10();
