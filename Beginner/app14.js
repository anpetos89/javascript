/*function saludarGomez(nombre) {
  console.log(`Hola ${nombre} Gomez`);
}

function saludarPerez(nombre) {
  console.log(`Hola ${nombre} Perez`);
}*/

function saludarFamilia(apellido) {
  return function saludarMiembroDeFamilia(nombre) {
    console.log(`Hola ${nombre} ${apellido}`);
  }
}

const saludarGomez = saludarFamilia("Gomez")
const saludarPerez = saludarFamilia("Perez")
const saludarRomero = saludarFamilia("Romero")

saludarGomez("Pedro")
saludarGomez("Juan")
saludarPerez("Laura")
saludarPerez("Mónica")
saludarRomero("Camilo")
saludarRomero("Tito")

function makePrefixer(prefijo) {
  return function prefix(sufijo) {
    console.log(`${prefijo}${sufijo}`);
  }
}

makePrefixer = prefijo => valor => prefijo+valor

const prefijoRe = makePrefixer("re")
prefijoRe("bueno")
