class Toggable {
  constructor(el) {
    // inicializar el estado interno
    this.el = el
    this.el.innerHTML = 'Off'
    this.activated = false
    this.onClick = this.onClick.bind(this)
    this.el.addEventListener('click', this.onClick)
  }
  onClick(ev) {
    // Cambiar el estado interno
    // llamar a toggleText
    this.activated = !this.activated
    this.toggleText()
  }
  toggleText() {
    // Cambiar el texto
    this.el.innerHTML = this.activated ? 'On' : 'Off'
  }
}

function saludar(nombre, apellido) {
  console.log(`Hola ${nombre} ${apellido}!`);
}

const saludarAndreses = saludar.bind(null, 'Andres')
saludarAndreses('Lozano')

const button = document.getElementById('button')

const myButton = new Toggable(button)
