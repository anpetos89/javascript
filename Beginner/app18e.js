// System.js
<script type="text/javascript" src="system.config.js"></script>
<script>
  System.config({
    paths: paths,
    map: map,
    packages: packages,
    transpiler: "ts",
    typescriptOptions: {
        tsconfig: true
    },
    meta: {
        typescript {
            exports: "ts"
        }
    }
  })
  System
    .import('app')
    .then(() => {

    })
    .catch((error) => {

    })
</script>
