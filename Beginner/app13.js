/*function suma(...numeros) {
  let acum = 0
  for (let i = 0; i < numeros.length; i++) {
    acum += numeros[i]
  }
  return acum
}*/

/*function suma(...numeros) {
  return numeros.reduce(function (acum, numero) {
    acum += numero
    return acum
  }, 0);
}*/

const suma = (...numeros) => numeros.reduce( (acum, numero) => acum += numero, 0 )

suma(4, 8, 12, 8954, 7, 9)

/*function dobles(...numeros) {
  const resultado = []
  for (let i = 0; i < numeros.length; i++) {
    resultado.push(numeros[i]*2)
  }
  return resultado
}*/

/*function dobles(...numeros) {
  return numeros.map(function (numero) {
    return numero * 2;
  })
}*/

/*function dobles(...numeros) {
  return numeros.map(numero => numero * 2)
}*/

const dobles = (...numeros) => numeros.map(numero => numero * 2)
dobles(3, 5, 10)

/*function pares(...numeros) {
  const resultado = []
  const length = numeros.length
  for (let i = 0; i < length; i++) {
    const numero = numeros[i]
    if (numero % 2 == 0) {
      resultado.push(numero)
    }
  }
  return resultado
}*/

/*function pares(...numeros) {
  return numeros.filter(function (numero) {
    return numero % 2 == 0
  })
}*/

const pares = (...numeros) => numeros.filter(numero => numero % 2 == 0 )

pares(3, 50, 10)
