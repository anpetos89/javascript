// Function scope
// No es muy efectivo para la parte de dependencias
var miModulo = (function miModulo() {
  var nombre = "Sacha"

  function setNombre(otroNombre) {
    nombre = otroNombre
  }
  function getNombre() {
    return nombre
  }

  return {
    setNombre,
    getNombre
  }
})()

miModulo.setNombre("Eric")
console.log(miModulo.getNombre());
