// AMD
// Sintaxis complicada
// triangle.js
define('triangle', function () {
  return function triangle(base = 0, height = 0) {
    return base * height / 2
  }
})

// otroarchivo.js
require('./triangle.js', function (triangle) {
  const base = 5
  const height = 7
  console.log(`El área de un triangulo de
    base ${base} y altura ${height} es: ${triangle(base, height)}`)
})
